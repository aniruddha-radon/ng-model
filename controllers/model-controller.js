app.controller("modelController", function ($scope) {
    //the $scope is the model. it acts as the glue between the view (HTML) and the controller.
    //Anything specified on the model is accessible in the view.
    //Anything NOT specified on the model is NOT accessible in the view.

    $scope.name = "Ash Ketchum"; // variable name is available in the view. // This is the default value
    var name = "Jane Doe"; // NOT available in the view.
})